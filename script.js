'use strict';



function factorial() {
    let num = prompt("Enter number");

    while(num === null || num==="" || isNaN(num) || num ==="0"){
    num = prompt("Enter number")
}

    let number = Number(num);
    let result=1;
    for(let i=1; i <= number; number--){
        result*=number;
    }

    console.log("RESULT", result);

    let div = document.createElement('div');
    document.body.append(div);

    let resultOfFactorial = document.createElement('h2');
    resultOfFactorial.textContent = `Factorial of ${num} is ${result}`;
    resultOfFactorial.style.fontSize = "40px"
    div.append(resultOfFactorial);
    return result;
}


factorial();
